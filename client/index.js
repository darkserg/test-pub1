const express = require('express')
const opn = require("opn");
const path = require('path');
const app = express();
const port = 80;
const url = `http://localhost:${port}`;

app.get('/', (req, res) => {
  res.sendFile(path.resolve('static/index.html'));
});

app.get('/axios.min.js', (req, res) => {
  res.sendFile(path.resolve('../node_modules/axios/dist/axios.min.js'));
});

app.listen(port, () => {
  console.log(`Listening at ${url}`)
  opn(url)
})
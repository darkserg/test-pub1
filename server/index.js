const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

app.post('/', (req, res) => {
  let body = req.body;
  let result = -1;
  let message = 'Incorrect name';

  if (body.name && body.name === 'Chip') {
    result = 0;
    message = undefined;
  }
  res.json({result, message})
})

app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`)
})